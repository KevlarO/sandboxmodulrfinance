package pageobjects;

import basepage.BaseActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Signin extends BaseActions {

    By usernameField = By.id("username-inp");
    By passwordField = By.id("password-inp");
    By signinSubmitButton = By.id("signInSubmitButton");
    By forgottenPassword = By.id("forgotPasswordHref");
    By resetAccessUsername = By.id("usernameInput");
    By usernameError = By.xpath("(//div[@class='alert alert-danger ng-star-inserted'])[1]");
    By passwordError = By.xpath("(//div[@class='alert alert-danger ng-star-inserted'])[2]");
    By lockNotification = By.xpath("//div[@class='alert alert-danger']");
    By resetAccessScreen = By.xpath("//p[@id='signInHeading']");
    By emailSentNotification = By.id("emailSentHeading");
    By accountsOverview = By.xpath("//p[contains(.,'Accounts')]");
    By accountAvatar = By.xpath("//p[contains(.,'Kevin Orukele')]");
    By signOutButton = By.xpath("//div/ul/li[text()=' Sign out ']");


    public static final String LOCK_MESSAGE = "The username or password is incorrect.\n" +
            "Multiple incorrect sign-ins could result in your access being locked. If this does happen, you'll receive an email explaining how to reset your access.";
    public static final String EXPECTED_USERNAME_AND_PASSWORD_ERROR = "This field is required";

    public Signin(WebDriver driver) {
        super(driver);
    }

    public Signin() {
        super();
    }

    public void enterSigninDetails(String username, String password){
        enter(usernameField, username);
        enter(passwordField, password);
        new Signin();
    }

    public void signin(){
        click(signinSubmitButton);
        new Signin();
    }

    public void getForgottenPassword(){
        click(forgottenPassword);
        new Signin();
    }

    public void enterResetAccessUsername(String username){
        enter(resetAccessUsername, username);
        new Signin();
    }

    public String getErrorNotificationText(String notificationToGet){

        switch(notificationToGet){
            case "username":
                waitForElementToAppear(usernameError);
                return getText(usernameError);
            case "password":
                waitForElementToAppear(passwordError);
                return getText(passwordError);
            case "lockMessage":
                waitForElementToAppear(lockNotification);
                return getText(lockNotification);
            default:
                return "incorrect selection";
        }

    }

    public boolean isRestAccessDisplayed(){

        return isDisplayed(resetAccessScreen);
    }

    public boolean isEmailSentDisplayed(){

        waitForElementToAppear(emailSentNotification);
        return isDisplayed(emailSentNotification);

    }

    public boolean isAccountOverviewPageDisplayed(){

        waitForElementToAppear(accountsOverview);

        return isDisplayed(accountsOverview);
    }

    public void signOut(){

        click(accountAvatar);
        click(signOutButton);
    }


}
