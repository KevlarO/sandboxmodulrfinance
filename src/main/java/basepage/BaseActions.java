package basepage;

import abstractcomponents.AbstractComponent;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

public class BaseActions extends AbstractComponent {

    protected WebDriver driver;

    public BaseActions(WebDriver driver) {

        this.driver = driver;
    }

    public BaseActions(){
        super();
    }

    @Override
    public WebElement findElement(By locator) {

        return driver.findElement(locator);
    }

    @Override
    public void click(By locator) {

        findElement(locator).click();
    }

    @Override
    public void enter(By locator, String textToEnter) {

        findElement(locator).sendKeys(textToEnter);
    }

    @Override
    public void refreshPage() {

        driver.navigate().refresh();
    }

    @Override
    public String getText(By locator) {

        return findElement(locator).getText();
    }

    @Override
    public boolean isDisplayed(By locator) {
        try {
            return findElement(locator).isDisplayed();
        } catch (org.openqa.selenium.NoSuchElementException exception){
            return false;
        }

    }

    // Wait utility
    @Override
    public void waitForElementToAppear(By locator) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    // Screenshot utility
    @Override
    public String getScreenShotPath(String testCaseName, WebDriver driver) throws IOException {

        // Path to store screenshot \\reports\\ -> folder
        String pathName = System.getProperty("user.dir") + "\\reports\\" + testCaseName + ".png";

        // Takes the screenshot
        TakesScreenshot ts =(TakesScreenshot) driver;

        // Creates a file and send the to the assigned destination
        FileHandler.copy(ts.getScreenshotAs(OutputType.FILE),new File(pathName));

        return pathName;
    }

}
