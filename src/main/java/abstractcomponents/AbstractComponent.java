package abstractcomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public abstract class AbstractComponent {

    // Test script actions
    public abstract WebElement findElement(By locator);

    public abstract void click(By locator);

    public abstract void enter(By locator, String textToEnter);

    public abstract void refreshPage();

    public abstract String getText(By locator);

    public abstract boolean isDisplayed(By locator);

    // Wait utility
    public abstract void waitForElementToAppear(By locator);

    // Screenshot utility

    public abstract String getScreenShotPath(String testCaseName, WebDriver driver) throws IOException;

}
