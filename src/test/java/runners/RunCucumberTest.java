package runners;

import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = {"src/test/java/features"},
        glue = {"StepDefinitions"},
        tags = "")
public class RunCucumberTest extends AbstractTestNGCucumberTests {

}
