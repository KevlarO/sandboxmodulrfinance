Feature: Signin Functionality

  Background:

  As a Modulr customer user
  I want to be able to login successfully to the Modulr Customer Portal
  So that I can manage my Modulr accounts

    Given Modulr customer user navigates to Customer Portal

    @Test1
  Scenario: Signin fields Validated successfully

    And clicks on Sign in
    Then Login process should fail

    @Test2
  Scenario: Successful password reset

    When The user clicks on forgotten password link
    Then The user is taken to request access screen
    And The user enters "TestUser"
    When The user clicks on request a reset
    Then Password request is successful

    @Test3
  Scenario: Invalid login

    When The user enters an invalid "Kevin.Orukele17" and "Testeraccount@87"
    And The user clicks on Sign in
    Then Login process should fail with the correct error message

    @Test4
  Scenario: Valid login

    And The user enters an valid "Kevin.Orukele16" and "Testeraccount@84"
    When The user clicks on Signin
    Then The user should be taken to the account overview page
