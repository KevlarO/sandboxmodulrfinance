package StepDefinitions;

import basepage.Base;
import pageobjects.Signin;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;

public class ValidLoginStep extends Base {

    Signin signin;
    private WebDriver driver;

    @Before("@Test4")
    public void setUp() throws IOException {
        driver = initialiseDriver();
        driver.get(prop.getProperty("url"));

    }

    @And("^The user enters an valid \"([^\"]*)\" and \"([^\"]*)\"$")
    public void the_user_enters_an_valid_something_and_something(String username, String password) {

        signin = new Signin(driver);
        signin.enterSigninDetails(username, password);
    }

    @When("^The user clicks on Signin$")
    public void the_user_clicks_on_signin() {

        signin.signin();
    }

    @Then("^The user should be taken to the account overview page$")
    public void the_user_should_be_taken_to_the_account_overview_page() {

        Assert.assertTrue(signin.isAccountOverviewPageDisplayed(),
                "Accounts overview page not displayed");

        signin.signOut();

    }

    @After("@Test4")
    public void tearDown(){

        driver.quit();
    }

}

