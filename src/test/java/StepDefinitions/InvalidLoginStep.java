package StepDefinitions;

import basepage.Base;
import pageobjects.Signin;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;

public class InvalidLoginStep extends Base {

    Signin signin;
    private WebDriver driver;

    @Before("@Test3")
    public void setUp() throws IOException {
        driver = initialiseDriver();
        driver.get(prop.getProperty("url"));

    }

    @When("^The user enters an invalid \"([^\"]*)\" and \"([^\"]*)\"$")
    public void the_user_enters_an_invalid_something_and_something(String username, String password) {
        signin = new Signin(driver);
        signin.enterSigninDetails(username, password);
    }

    @And("^The user clicks on Sign in$")
    public void the_user_clicks_on_sign_in() {

        signin.signin();
    }

    @Then("^Login process should fail with the correct error message$")
    public void login_process_should_fail_with_the_correct_error_message() {

        Assert.assertEquals(signin.getErrorNotificationText("lockMessage"), Signin.LOCK_MESSAGE,
                "The Actual result is different from the expected result. ->");
    }

    @After("@Test3")
    public void tearDown(){

        driver.quit();
    }

}
