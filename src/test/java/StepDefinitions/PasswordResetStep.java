package StepDefinitions;

import basepage.Base;
import pageobjects.Signin;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;

public class PasswordResetStep extends Base {

    Signin signin;
    private WebDriver driver;

    @Before("@Test2")
    public void setUp() throws IOException {
        driver = initialiseDriver();
        driver.get(prop.getProperty("url"));

    }

    @When("^The user clicks on forgotten password link$")
    public void the_user_clicks_on_forgotten_password_link() {

        signin = new Signin(driver);
        signin.getForgottenPassword();
    }

    @Then("^The user is taken to request access screen$")
    public void the_user_is_taken_to_request_access_screen()  {

        Assert.assertTrue(signin.isRestAccessDisplayed());
    }

    @And("^The user enters \"([^\"]*)\"$")
    public void the_user_enters_something(String username){

        signin.enterResetAccessUsername(username);
    }

    @When("^The user clicks on request a reset$")
    public void the_user_clicks_on_request_a_reset()  {

        signin.signin();
    }

    @Then("^Password request is successful$")
    public void password_request_is_successful() {

        Assert.assertTrue(signin.isEmailSentDisplayed());
    }

    @After("@Test2")
    public void tearDown(){

        driver.quit();
    }

}
