package StepDefinitions;

import basepage.Base;
import pageobjects.Signin;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;

public class FieldsValidationStep extends Base {

    Signin signin;
    protected WebDriver driver;

    @Before("@Test1")
    public void setUp() throws IOException {
        driver = initialiseDriver();
        driver.get(prop.getProperty("url"));

    }

    @Given("^Modulr customer user navigates to Customer Portal$")
    public void modulr_customer_user_navigates_to_Customer_Portal() {
        signin = new Signin(driver);

    }

    @And("^clicks on Sign in$")
    public void clicks_on_sign_in() {

        signin.signin();
    }

    @Then("^Login process should fail$")
    public void login_process_should_fail() {

        Assert.assertEquals(signin.getErrorNotificationText("username"),
                Signin.EXPECTED_USERNAME_AND_PASSWORD_ERROR,
                "The Actual result is different from expected result. ->");

        Assert.assertEquals(signin.getErrorNotificationText("password"),
                Signin.EXPECTED_USERNAME_AND_PASSWORD_ERROR,
                "The Actual result is different from expected result. ->");

    }

    @After("@Test1")
    public void tearDown(){

        driver.quit();
    }

}
